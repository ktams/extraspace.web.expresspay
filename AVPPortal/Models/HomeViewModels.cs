﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using AVPPortal.Tools;
using System.Web.Mvc;

namespace AVPPortal.Models
{
    public class LoginViewModel
    {
        public int Attempts { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Vendor Identification")]
        public string VendorId { get; set; }

        [Required(ErrorMessage = "Required")]
        [MaxLength(4)]
        [Display(Name = "Tax ID or Social")]
        public string Password { get; set; }
    }

    public class SignupViewModel
    {
        // Payment Method Fields
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Payment Method")]
        public string PaymentMethod { get; set; }

        [ValidateConditionalRequired(ConditionalField = "PaymentMethod", ConditionalOperator = "NEQ", ConditionalValue = "ACH", ErrorMessage = "Required")]
        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

        [MaxLength(9)]
        [ValidateRoutingNumber]
        [RegularExpression(@"^\d+$", ErrorMessage = "Invalid Routing Number")]
        [ValidateConditionalRequired(ConditionalField = "PaymentMethod", ConditionalOperator = "NEQ", ConditionalValue = "ACH", ErrorMessage = "Required")]
        [Display(Name = "Routing Number")]
        public string RoutingNumber { get; set; }

        [MaxLength(15)]
        [RegularExpression(@"^[x0-9]+$", ErrorMessage = "Invalid Account Number")]
        [ValidateConditionalRequired(ConditionalField = "PaymentMethod", ConditionalOperator = "NEQ", ConditionalValue = "ACH", ErrorMessage = "Required")]
        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        // Vendor Information Fields
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Vendor/Company Name")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Primary Contact")]
        public string PrimaryContact { get; set; }

        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Invalid Email")]
        [ValidateEmail(ErrorMessage = "Invalid Email.")]
        [Display(Name = "Primary Contact Email")]
        public string PrimaryContactEmail { get; set; }

        [Required(ErrorMessage = "Required")]
        [Phone(ErrorMessage = "Invalid Phone Number.")]
        [ValidatePhone(ErrorMessage = "Invalid Phone Number")]
        [Display(Name = "Primary Contact Phone")]
        public string PrimaryContactPhone { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Tax Registration")]
        public string TaxRegistration { get; set; }
        public SelectList TaxRegistrationSelector
        {
            get
            {
                SelectList registration = new SelectList(
                    new List<object>
                    {
                        new { value = "Attorney", text = "Attorney"},
                        new { value = "Corporation", text = "Corporation"},
                        new { value = "Individual", text = "Individual"},
                        new { value = "Limited Liability Company", text = "Limited Liability Company"},
                        new { value = "Other", text = "Other"},
                        new { value = "Partnership", text = "Partnership"},
                        new { value = "Sole Proprietor", text = "Sole Proprietor"},
                        new { value = "Trust", text = "Trust"},
                        new { value = "Settlement", text = "Settlement"},
                        new { value = "Unknown", text = "Unknown"},
                        new { value = "Bank", text = "Bank"},
                        new { value = "Corporate Card", text = "Corporate Card"},
                        new { value = "Distribution", text = "Distribution"},
                        new { value = "Employee", text = "Employee"},
                        new { value = "General Contractor", text = "General Contractor"},
                        new { value = "Government Agency", text = "Government Agency"},
                        new { value = "Insurance", text = "Insurance"},
                        new { value = "Tenant Refund", text = "Tenant Refund"},
                        new { value = "Utilities", text = "Utilities"}
                    }, "value", "text", TaxRegistration
                );

                return registration;
            }
        }

        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"^[x\-0-9]+$", ErrorMessage = "Invalid Tax ID")]
        [Display(Name = "Tax ID")]
        public string TaxID { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Street Address")]
        public string StreetAddress { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "State")]
        public string State { get; set; }
        public SelectList StateSelector
        {
            get
            {
                List<object> stateList = new List<object>();
                DataTable dtStates = new DataTable();

                using(SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    con.Open();
                    SqlCommand getStates = new SqlCommand("SELECT DISTINCT StateAbbreviation, StateName FROM State", con);
                    SqlDataAdapter da = new SqlDataAdapter(getStates);
                    da.Fill(dtStates);
                }

                foreach(DataRow drState in dtStates.Rows)
                {
                    stateList.Add(new { value = drState["StateAbbreviation"], text = drState["StateAbbreviation"] });
                }

                SelectList state = new SelectList(stateList, "value", "text", State);

                return state;
            }
        }

        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"(\d{5}([\-]\d{4})?)", ErrorMessage = "Invalid Zip Code")]
        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Terms")]
        public bool Agree { get; set; }
    }
}