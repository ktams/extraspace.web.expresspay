﻿using System;
using System.Web;
using System.Web.Mvc;

namespace AVPPortal.Tools
{
    public class RequireSSL : ActionFilterAttribute
    {
        public bool HTTPSRequired { get; set; }

        //public override void OnActionExecuting(ActionExecutingContext filterContext)
        //{
        //    HttpRequestBase req = filterContext.HttpContext.Request;
        //    HttpResponseBase res = filterContext.HttpContext.Response;
            
        //    if (HTTPSRequired && !req.IsSecureConnection)
        //    {
        //        var builder = new UriBuilder(req.Url)
        //        {
        //            Scheme = Uri.UriSchemeHttps,
        //            Port = 443
        //        };

        //        if (req.IsLocal)
        //        {
        //            builder.Port = 44302;
        //        }

        //        filterContext.Result = new RedirectResult(builder.Uri.ToString());
        //    }

        //    if (!HTTPSRequired && req.IsSecureConnection)
        //    {
        //        var builder = new UriBuilder(req.Url)
        //        {
        //            Scheme = Uri.UriSchemeHttp,
        //            Port = 80
        //        };

        //        if (req.IsLocal)
        //        {
        //            builder.Port = 49814;
        //        }

        //        filterContext.Result = new RedirectResult(builder.Uri.ToString());
        //    }

        //    base.OnActionExecuting(filterContext);
        //}
    }
}