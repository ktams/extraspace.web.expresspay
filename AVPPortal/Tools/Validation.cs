﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace AVPPortal.Tools
{
    public class ValidateBoolAttribute : ValidationAttribute, IClientValidatable
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));

            if (value.GetType() != typeof(bool))
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));

            if ((bool)value == true)
                return ValidationResult.Success;
            else
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }

        private static T GetValue<T>(object objectInstance, string propertyName)
        {
            if (objectInstance == null) throw new ArgumentNullException("objectInstance");
            if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentNullException("propertyName");

            var propertyInfo = objectInstance.GetType().GetProperty(propertyName);
            return (T)propertyInfo.GetValue(objectInstance);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var modelClientValidationRule = new ModelClientValidationRule
            {
                ValidationType = "validateemail",
                ErrorMessage = FormatErrorMessage(metadata.DisplayName)
            };

            yield return modelClientValidationRule;
        }
    }

    public class ValidateEmailAttribute : ValidationAttribute, IClientValidatable
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                string email = value.ToString().ToLower();
                if (email == "any@any.com" || email == "none@none.com" || email == "example@example.com" || email == "unknown@unknown.com" || email == "test@test.com" || email == "noemail@nomail.com" || email == "refused@refused.com" || email == "nomail@nomail.com")
                {
                    return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                }
            }

            return ValidationResult.Success;
        }

        private static T GetValue<T>(object objectInstance, string propertyName)
        {
            if (objectInstance == null) throw new ArgumentNullException("objectInstance");
            if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentNullException("propertyName");

            var propertyInfo = objectInstance.GetType().GetProperty(propertyName);
            return (T)propertyInfo.GetValue(objectInstance);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var modelClientValidationRule = new ModelClientValidationRule
            {
                ValidationType = "validateemail",
                ErrorMessage = FormatErrorMessage(metadata.DisplayName)
            };

            yield return modelClientValidationRule;
        }
    }

    public class ValidatePhoneAttribute : ValidationAttribute, IClientValidatable
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string phone = Regex.Replace(value.ToString(), "[^0-9]", "");

            long tempPhone;
            if (!Int64.TryParse(phone, out tempPhone) || phone.Length != 10)
            {
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            }

            return ValidationResult.Success;
        }

        private static T GetValue<T>(object objectInstance, string propertyName)
        {
            if (objectInstance == null) throw new ArgumentNullException("objectInstance");
            if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentNullException("propertyName");

            var propertyInfo = objectInstance.GetType().GetProperty(propertyName);
            return (T)propertyInfo.GetValue(objectInstance);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var modelClientValidationRule = new ModelClientValidationRule
            {
                ValidationType = "validatephone",
                ErrorMessage = FormatErrorMessage(metadata.DisplayName)
            };

            yield return modelClientValidationRule;
        }
    }

    public class ValidateRoutingNumberAttribute : ValidationAttribute, IClientValidatable
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            bool valid = true;

            if (value != null)
            {
                // Validate first two
                string firstTwo = value.ToString().Substring(0, 2);
                if (firstTwo == "01" || firstTwo == "02" || firstTwo == "03" || firstTwo == "04" || firstTwo == "05" || firstTwo == "06" || firstTwo == "07" || firstTwo == "08" || firstTwo == "09" || firstTwo == "10" || firstTwo == "11" || firstTwo == "12" || firstTwo == "21" || firstTwo == "22" || firstTwo == "23" || firstTwo == "24" || firstTwo == "25" || firstTwo == "26" || firstTwo == "27" || firstTwo == "28" || firstTwo == "29" || firstTwo == "30" || firstTwo == "31" || firstTwo == "32" || firstTwo == "61" || firstTwo == "62" || firstTwo == "63" || firstTwo == "64" || firstTwo == "65" || firstTwo == "66" || firstTwo == "67" || firstTwo == "68" || firstTwo == "69" || firstTwo == "70" || firstTwo == "71" || firstTwo == "72")
                {
                    // Check Prime
                    int i = 0;
                    string t = "";
                    for (i = 0; i < value.ToString().Length; i++)
                    {
                        int c = Int32.Parse(value.ToString()[i].ToString());
                        if (c >= 0 && c <= 9)
                        {
                            t = t + c;
                        }
                    }

                    int n = 0;
                    for (i = 0; i < value.ToString().Length; i += 3)
                    {
                        n += Int32.Parse(t[i].ToString()) * 3 + Int32.Parse(t[i + 1].ToString()) * 7 + Int32.Parse(t[i + 2].ToString());
                    }

                    if (n != 0 && n % 10 == 0 && t.Length == 9)
                    {
                        valid = true;
                    }
                    else
                    {
                        valid = false;
                    }
                }
                else
                {
                    valid = false;
                }
            }

            if (valid)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            }
        }

        private static T GetValue<T>(object objectInstance, string propertyName)
        {
            if (objectInstance == null) throw new ArgumentNullException("objectInstance");
            if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentNullException("propertyName");

            var propertyInfo = objectInstance.GetType().GetProperty(propertyName);
            return (T)propertyInfo.GetValue(objectInstance);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var modelClientValidationRule = new ModelClientValidationRule
            {
                ValidationType = "validateroutingnumber",
                ErrorMessage = FormatErrorMessage(metadata.DisplayName)
            };

            yield return modelClientValidationRule;
        }
    }

    public class ValidateConditionalRequired : ValidationAttribute, IClientValidatable
    {
        public string ConditionalField { get; set; }
        public string ConditionalValue { get; set; }
        public string ConditionalOperator { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            switch (ConditionalOperator)
            {
                case "EQ":
                    // Return error if condition is not equal to value
                    if (GetValue<string>(validationContext.ObjectInstance, ConditionalField) != ConditionalValue)
                    {
                        if (value == null || value.ToString().Length == 0)
                        {
                            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                        }
                    }
                    break;
                case "NEQ":
                    // Return error if condition is equal to value
                    if (GetValue<string>(validationContext.ObjectInstance, ConditionalField) == ConditionalValue)
                    {
                        if (value == null || value.ToString().Length == 0)
                        {
                            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                        }
                    }
                    break;
                case "LT":
                    // Return error if condition is greater than or equal to value
                    if (Double.Parse(GetValue<string>(validationContext.ObjectInstance, ConditionalField)) >= Double.Parse(ConditionalValue))
                    {
                        if (value == null || value.ToString().Length == 0)
                        {
                            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                        }
                    }
                    break;
                case "GT":
                    // Return error if condition is less than or equal to value
                    if (Double.Parse(GetValue<string>(validationContext.ObjectInstance, ConditionalField)) <= Double.Parse(ConditionalValue))
                    {
                        if (value == null || value.ToString().Length == 0)
                        {
                            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                        }
                    }
                    break;
                case "LTEQ":
                    // Return error if condition is greater than value
                    if (Double.Parse(GetValue<string>(validationContext.ObjectInstance, ConditionalField)) > Double.Parse(ConditionalValue))
                    {
                        if (value == null || value.ToString().Length == 0)
                        {
                            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                        }
                    }
                    break;
                case "GTEQ":
                    // Return error if condition is less than value
                    if (Double.Parse(GetValue<string>(validationContext.ObjectInstance, ConditionalField)) < Double.Parse(ConditionalValue))
                    {
                        if (value == null || value.ToString().Length == 0)
                        {
                            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                        }
                    }
                    break;
                default:
                    // If no operator is specified return error if condition is not equal to value
                    if (GetValue<string>(validationContext.ObjectInstance, ConditionalField) != ConditionalValue)
                    {
                        if (value == null || value.ToString().Length == 0)
                        {
                            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
                        }
                    }
                    break;
            }

            return ValidationResult.Success;
        }

        private static T GetValue<T>(object objectInstance, string propertyName)
        {
            if (objectInstance == null) throw new ArgumentNullException("objectInstance");
            if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentNullException("propertyName");

            var propertyInfo = objectInstance.GetType().GetProperty(propertyName);
            return (T)propertyInfo.GetValue(objectInstance);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var modelClientValidationRule = new ModelClientValidationRule
            {
                ValidationType = "validateconditionalrequired",
                ErrorMessage = FormatErrorMessage(metadata.DisplayName)
            };

            modelClientValidationRule.ValidationParameters.Add("validateconditionalrequired", ConditionalField);
            modelClientValidationRule.ValidationParameters.Add("validateconditionalrequiredvalue", ConditionalValue);
            yield return modelClientValidationRule;
        }
    }
}