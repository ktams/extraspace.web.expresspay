﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Data.SqlClient;
using AVPPortal.Models;
using AVPPortal.Tools;
using CodeBase;

namespace AVPPortal.Controllers
{
    [RequireSSL(HTTPSRequired = true)]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = TempData["ErrorMessage"];
            }

            LoginViewModel model = new LoginViewModel();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // First, see if user is locked out
                    using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        con.Open();
                        SqlCommand lockoutInfo = new SqlCommand("SELECT LockoutTime FROM VendorLoginAttempts WHERE VendorID = @VendorID", con);
                        lockoutInfo.Parameters.Add(new SqlParameter("@VendorID", model.VendorId));
                        try
                        {
                            DateTime lockoutTime = (DateTime)lockoutInfo.ExecuteScalar();
                            if (lockoutTime > DateTime.Now.AddMinutes(-30)) {
                                model.Attempts = 4;
                                return View(model);
                            }
                        }
                        catch (Exception) { }                        
                    }

                    // Now check credentials in SFDC
                    string accountID = CodeBase.Salesforce.VendorLogin((CodeBase.Config)HttpContext.Application["Config"], model.VendorId, model.Password);
                    if (accountID != null)
                    {
                        // successful login
                        Session["AccountID"] = accountID;

                        // reset login attempts to 0 and clear lockout time
                        using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                        {
                            con.Open();
                            SqlCommand undoLockout = new SqlCommand("UPDATE VendorLoginAttempts SET LockoutTime = null, FailedAttempts = 0 WHERE VendorID = @VendorID", con);
                            undoLockout.Parameters.Add(new SqlParameter("@VendorID", model.VendorId));
                            undoLockout.ExecuteNonQuery();
                        }

                        return RedirectToAction("Signup", "Home");
                    }
                    else
                    {
                        // login failed
                        using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                        {
                            // increment attempt count or insert row if vendor doesn't exist
                            con.Open();
                            StringBuilder sb = new StringBuilder();
                            sb.Append("with LoginAttempts as ");
                            sb.Append("( ");
                            sb.Append("	select @VendorID as VendorID ");
                            sb.Append(") ");
                            sb.Append("merge into VendorLoginAttempts target ");
                            sb.Append("using LoginAttempts source ");
                            sb.Append("on source.VendorID = target.VendorID ");
                            sb.Append("when matched then update set target.FailedAttempts = target.FailedAttempts + 1 ");
                            sb.Append("when not matched then insert values (@VendorID, 1, null) ");
                            sb.Append("output inserted.FailedAttempts; ");
                            SqlCommand loginInfo = new SqlCommand(sb.ToString(), con);
                            loginInfo.Parameters.Add(new SqlParameter("@VendorID", model.VendorId));
                            int attemptCount = (int)loginInfo.ExecuteScalar();
                            
                            model.Attempts = attemptCount;
                            if (attemptCount == 4)
                            {
                                // lock user out for 30 minutes
                                SqlCommand setLockout = new SqlCommand("UPDATE VendorLoginAttempts SET LockoutTime = CURRENT_TIMESTAMP, FailedAttempts = 0 WHERE VendorID = @VendorID", con);
                                setLockout.Parameters.Add(new SqlParameter("@VendorID", model.VendorId));
                                setLockout.ExecuteNonQuery();

                                // notify accounting of the lockout
                                CodeBase.Tools.SendEmail((CodeBase.Config)HttpContext.Application["Config"], "expresspay@extraspace.com", "Vendor locked out of ExpressPay", 
                                    string.Format("This vendor made 4 failed login attempts and has been locked out for the next 30 minutes.<br /><br />Vendor ID: {0}<br />IP Address: {1}", 
                                    model.VendorId, Request.UserHostAddress));
                            }
                        }

                        ModelState.AddModelError("Password", "Login failed. Please try again.");
                    }
                }
                catch (Exception ex)
                {
                    CodeBase.Tools.SendMailEx((CodeBase.Config)HttpContext.Application["Config"], ex, "Vendor login failed", "VendorID: " + model.VendorId);
                }
            }

            return View(model);
        }

        public ActionResult Logout()
        {
            // Logout and redirect to homepage
            Session.RemoveAll();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Signup()
        {
            // Check if user is logged in
            if (Session["AccountID"] != null)
            {
                try
                {
                    // Get Vendor info from Salesforce
                    Vendor vendor = CodeBase.Salesforce.GetVendorInfo((CodeBase.Config)HttpContext.Application["Config"], Session["AccountID"].ToString());

                    SignupViewModel model = new SignupViewModel()
                    {
                        PaymentMethod = vendor.PaymentMethod,
                        BankName = vendor.BankName,
                        RoutingNumber = vendor.RoutingNumber,
                        AccountNumber = vendor.AccountNumber,
                        CompanyName = vendor.VendorName,
                        PrimaryContact = vendor.PrimaryContact,
                        PrimaryContactEmail = vendor.Email,
                        PrimaryContactPhone = vendor.Phone,
                        TaxRegistration = vendor.TaxRegistration,
                        TaxID = vendor.TaxID,
                        StreetAddress = vendor.Address,
                        City = vendor.City,
                        State = vendor.StateAbbr,
                        ZipCode = vendor.Zip
                    };

                    return View(model);
                }
                catch (Exception)
                {
                    // Couldn't load Vendor info, redirect to homepage
                    TempData["ErrorMessage"] = "There was a problem";
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                // User is not logged in, redirect to homepage
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Signup(SignupViewModel model)
        {
            // Check if user is logged in
            if (Session["AccountID"] != null)
            {
                // Check if terms were accepted
                if (model.Agree != true)
                    ModelState.AddModelError("Agree", "Required");

                if (ModelState.IsValid)
                {
                    try
                    {
                        Vendor vendor = new Vendor()
                        {
                            PaymentMethod = model.PaymentMethod,
                            BankName = model.BankName,
                            RoutingNumber = model.RoutingNumber,
                            AccountNumber = model.AccountNumber,
                            VendorName = model.CompanyName,
                            PrimaryContact = model.PrimaryContact,
                            Email = model.PrimaryContactEmail,
                            Phone = model.PrimaryContactPhone,
                            TaxRegistration = model.TaxRegistration,
                            TaxID = model.TaxID,
                            Address = model.StreetAddress,
                            City = model.City,
                            StateAbbr = model.State,
                            Zip = model.ZipCode
                        };

                        bool updated = CodeBase.Salesforce.UpdateVendorInfo((CodeBase.Config)HttpContext.Application["Config"], Session["AccountID"].ToString(), vendor);

                        if (updated)
                        {
                            // send plain text confirmation email to vendor
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<strong>Welcome to Extra Space Storage EXPRESS PAY!<br />The fastest way to be paid.</strong><br /><br />");
                            sb.AppendFormat("This is a confirmation that we received your request to be paid via {0}. Your preferred payment method will be applied to your next submitted invoice.<br /><br />", model.PaymentMethod.ToUpper());
                            sb.Append("We appreciate the service you provide Extra Space Storage!<br />");
                            // only show this link until June 10
                            if (DateTime.Now < new DateTime(2014, 6, 11))
                                sb.Append("- <a href=\"http://extraspace.qualtrics.com/SE/?SID=SV_0IgRWsBeNv51Pfv\">Click here to claim your $25 gift card.</a><br /><br /><br />");
                            else
                                sb.Append("<br /><br />");
                            sb.Append("<strong>Payment & Account Information:</strong><hr />");
                            sb.AppendFormat("<strong>Preferred Payment Method:</strong> {0}<br />", model.PaymentMethod);
                            sb.AppendFormat("<strong>Company/Vendor Name:</strong> {0}<br />", model.CompanyName);
                            sb.AppendFormat("<strong>Primary Contact:</strong> {0}<br />", model.PrimaryContact);
                            sb.AppendFormat("<strong>Primary Contact Email:</strong> {0}<br />", model.PrimaryContactEmail);
                            sb.AppendFormat("<strong>Primary Contact Phone:</strong> {0}<br />", model.PrimaryContactPhone);
                            sb.AppendFormat("<strong>Tax Registration:</strong> {0}<br />", model.TaxRegistration);
                            sb.AppendFormat("<strong>Tax ID:</strong> {0}<br />", CodeBase.Tools.MaskSensitiveNumber(model.TaxID));
                            sb.AppendFormat("<strong>Street Address:</strong> {0}<br />", model.StreetAddress);
                            sb.AppendFormat("<strong>City:</strong> {0}<br />", model.City);
                            sb.AppendFormat("<strong>State:</strong> {0}<br />", model.State);
                            sb.AppendFormat("<strong>Zip Code:</strong> {0}<br />", model.ZipCode);
                            sb.Append("<a href=\"https://expresspay.extraspace.com\">Click here to edit your information</a><br /><br /><br /><hr />");
                            sb.Append("Should you have any questions, email <a href=\"mailto:expresspay@extraspace.com\">expresspay@extraspace.com</a> or call <strong>855-229-8577</strong>.");
                            CodeBase.Tools.SendEmail((CodeBase.Config)HttpContext.Application["Config"], model.PrimaryContactEmail, "Extra Space Storage EXPRESS PAY Confirmation", sb.ToString());
                            
                            return RedirectToAction("ThankYou", "Home");
                        }
                        else
                        {
                            ViewBag.ErrorMessage = "There was a problem updating your information. Please try again or contact us at 855-229-8577.";
                        }
                    }
                    catch(Exception)
                    {
                        ViewBag.ErrorMessage = "There was a problem updating your information. Please try again or contact us at 855-229-8577.";
                    }
                }

                return View(model);
            }
            else
            {
                // User is not logged in, redirect to homepage
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult ThankYou()
        {
            // Check if user is logged in
            if (Session["AccountID"] != null)
            {
                Session.RemoveAll();
                return View();
            }
            else
            {
                // User is not logged in, redirect to homepage
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult KeepSFAlive()
        {
            // Make a simple Salesforce call to keep the session alive
            try
            {
                CodeBase.Salesforce.login((CodeBase.Config)HttpContext.Application["Config"]);
            }
            catch (Exception) { }

            // Redirect to homepage
            return RedirectToAction("Index", "Home");
        }
    }
}